# BTU.js

## Convert BTUs to Watts:

```javascript
BTU.toWatts(5000)
```

## Convert Watts to BTUs:

```javascript
BTU.fromWatts(1465.35)
```
