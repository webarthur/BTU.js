// 10/2017
// Arthur Araújo <webarthur@gmail.com>

var BTU = {

  factor: .293071,

  fromWatts: function (watts, round) {
    return round ?  Math.round(watts / BTU.factor) : watts / BTU.factor
  },

  toWatts: function (btu, round) {
    return round ?  Math.round(btu * BTU.factor) : btu * BTU.factor
  }

}

if (module) module.exports = BTU
